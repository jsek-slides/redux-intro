# Redux - with middleware

Presentation about the right way of using Redux

Live at [redux-done-right.surge.sh](https://redux-done-right.surge.sh)

## Setup

- Run `yarn`

### Run on localhost

```shell
yarn dev
```

---

## Outline

- My current project - playground for Redux
- What is Redux?
  - Principles
  - Very brief look at ecosystem (alternatives, implementations, devTools)
  - Redux is not a free ride
  - Why you might need Redux?
- Redux is simple
  - store, API, reducer
  - example
  - concept explanation
- Demo: minimum setup for Redux / Vuex / Ngrx
- Hypothetical development flow
- Our story of adoption
- Demo: real life - how if feels to use Redux in our project
- 3 Problems
- Direction of our efforts
- Middleware
  - benefits
  - explanation (hammer, pacman)
  - examples
- 3 Tips
- There is more... questions
- Redux tips - summary
- Resources

---

## Unused links

- [How We Ditched Redux for MobX](https://medium.com/skillshare-team/how-we-ditched-redux-for-mobx-a05442279a2b)
- [Understanding Redux Middleware And Writing Custom Ones](https://designingforscale.com/understanding-redux-middleware-and-writing-custom-ones/)
- [Developing modern offline apps with ReactJS, Redux and Electron](https://blog.codecentric.de/en/2017/12/developing-modern-offline-apps-reactjs-redux-electron-part-3-reactjs-redux-basics/)
- [State management](https://www.google.pl/imgres?imgurl=http%3A%2F%2Fkrasimirtsonev.com%2Fblog%2Farticles%2Fstate-management%2Ftable.jpg&imgrefurl=http%3A%2F%2Fkrasimirtsonev.com%2Fblog%2Farticle%2Fgetting-from-redux-to-state-machine-with-stent&docid=lMP6wFKGoQecgM&tbnid=WaI0M73yBtfXtM%3A&vet=12ahUKEwi37qu71NndAhVCXiwKHWYOBL84ZBAzKAcwB3oECAEQCA..i&w=800&h=302&bih=1296&biw=1270&q=redux%20graph&ved=2ahUKEwi37qu71NndAhVCXiwKHWYOBL84ZBAzKAcwB3oECAEQCA&iact=mrc&uact=8)
- [Login page state machine](https://www.google.pl/imgres?imgurl=https%3A%2F%2Fcdn.artandlogic.com%2Fwp-content%2Fuploads%2F2013%2F06%2Flogininterfacestatemachine.png&imgrefurl=https%3A%2F%2Fartandlogic.com%2F2013%2F06%2Frepresenting-marionette-js-views-with-state%2Flogininterfacestatemachine-png%2F&docid=QdDQPUNG-NMreM&tbnid=wfA01jUFBp3HvM%3A&vet=1&w=1501&h=620&bih=1297&biw=1272&ved=2ahUKEwiPg4rl0ffdAhUIhaYKHaXdCrgQxiAoAXoECAEQFQ&iact=c&ictx=1)
- [Where Flux went wrong](http://technologyadvice.github.io/where-flux-went-wrong/)
- [Ngrx Tips & Tricks](https://blog.angularindepth.com/ngrx-tips-tricks-69feb20a42a7)
- [Redux dataflow](https://blog.isquaredsoftware.com/presentations/2018-03-redux-fundamentals/#/19)

<!-- https://egghead.io/lessons/react-redux-extracting-container-components-filterlink -->