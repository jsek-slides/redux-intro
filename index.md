<!-- Dark+ Material -->

# ![Redux](img/1_logo.png)

## with Middleware

<hr>

<!-- Key takeaway: Middleware is essential to avoid chaos -->

---

<br>

We are in post-MV* world. <hr> Client-side is composed with components.

<br>

---

![](pics/wallhaven-640609.jpg)

<notes>
New challanges
- scaling
- routing
- dependency management
- state management
</notes>

---

- Origins
- API / Example
- Our story
- Tips

---

## What is Redux?

> <g>Relatively young tool for </g><br>state management<g><br> in</g> `JavaScript` <g>that helps</g><br> _**decouple logic from view**_

<!-- 3 years -->

<small>inspired by Flux</small>

<small>created by Dan Abramov</small>

---

### Redux [principles](https://redux.js.org/introduction/threeprinciples)

* Single source of truth
* Read-only state
* Events are consumed by pure functions

---

#### Who benefits?

> Developers

<hr>

###### Think "selfish"<br><small>If it is not helping, you are holding it wrong</small>

<!-- To podejście procentuje - stopniowo coraz więcej z niego pożytku -->

---

### Interesting fact

Once an "option" for *Backbone.js* users

---

![](img/backbone.png)

---

### Alternatives

- [Flux](https://facebook.github.io/flux)

<!-- Consider if you use GraphQL -->

- [Relay](https://facebook.github.io/relay/)
- [Falcor](https://netflix.github.io/falcor/) 

<!-- GraphQL + Apollo may be alternative in some cases -->

---

You can use `redux` library in any project<br/>
<small>`> npm i redux`</small>

<br/>or choose dedicated adaptations:

- Angular-Redux
- ~~[Ngrx](https://github.com/ngrx)~~
- [NGXS](https://ngxs.gitbook.io/ngxs/)
- [Vuex](https://vuex.vuejs.org/) <!-- for Vue.js -->

---

### DevTools

- [Redux DevTools](http://extension.remotedev.io/)
- [Reactotron](https://github.com/infinitered/reactotron)
- [Vue DevTools](https://github.com/vuejs/vue-devtools)

<!-- https://vuex.vuejs.org/api/#devtools -->

---

<br>
<br>

> <i>It *__got very popular very quickly__*, and as a result, a lot of __*people were told they had to use Redux*__, without actually understanding the *tradeoffs* involved and *when it actually makes sense* to use Redux.</i>
> <br><br> <small>[Redux not dead yet](https://blog.isquaredsoftware.com/2018/03/redux-not-dead-yet/)</small>

<br>
<br>

---

<!-- Let's assume you try to use one of them -->
<!-- It appears that there is a knowledge gap -->

## Cost: Learning curve - Lingo

```grid(4:4,#01579b:#083d69,gap:.3em)
State
Store

Event
Dispatcher

Action
Action creator

Pure function
Side-effect

Reducer
Selector

Middleware
Enhancer

Thunk
Saga

Caching
...
```

<small>Docs &gt; [Glossary](https://redux.js.org/glossary)</small>

---

## Cost: Redux ecosystem - noise

<br>

- [redux-ecosystem-links](https://github.com/markerikson/redux-ecosystem-links)
<br><small>~ 2500</small>
- [react-redux-links](https://github.com/markerikson/react-redux-links)
<br><small>~ 2600</small>

<br>

<notes>
The problem:
- lots of noise
- examples rarely fit our needs
</notes>

---

### _<i class="fa fa-balance-scale"></i>_ Every new complexity must be justified

No benefits? No deal <i class="fa fa-shopping-cart"></i>

---

### So first, lets understand origins

<br>
<br>

Well... Facebook had a problem

<small>Status change propagation in big SPA</small>

---

![](img/complications.png)

---

### Question to ask yourself

_How hard it is to..._
 - add new component
 - split a component that grown to big

<hr>

_How hard_ = how many __files to read__
  <br> &nbsp; &nbsp; <small><i class='fa fa-plus'></i></small> how many __scenarios to check__
  <br> &nbsp; &nbsp; <small><i class='fa fa-plus'></i></small> how many times you __rewrite__ <small>(open/close)</small>
  <br> &nbsp; &nbsp; <small><i class='fa fa-plus'></i></small> how many __worries about maintainability__

<notes>
Ask as "As developer, how many files..."

I am too lazy to check every scenario - so how many defects I generate?

How hard it is -vs- how hard it seems it should be?

Yep - I started to worry about maintainability after few months of development
</notes>

---

### What makes it harder?

- 2-way data binding
<br/><small>when we want to propagate input</small>
<!-- not harmful until you overuse `broadcast` / `emit` -->
<!-- hence we used to broadcast from `$rootStore` in Angular.js -->


- Business logic in components
<!-- splitted components -> splitted logic -> regression risk -->

---

![](img/with-without.png)

---

#### What is *__store__*?

---

![](img/store-code.png)

---

![](img/store-graph.png)

---

```impact
REDUX is &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;◂
SIMPLE
▸&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; in theory
```

---

```impact,narrow
baseline API
⮡ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4 methods
```

---

*`createStore(`*<br/><span style='width:1em;display:inline-block'></span>`reducer,`<br><span style='width:1em;display:inline-block'></span>`initialState,`<br><span style='width:1em;display:inline-block'></span>`middleware`<br>*`)`*

<br>`store.`*`dispatch(`*`event`*`)`*

`store.`*`subscribe(`*`listener`*`)`*

`store.`*`getState()`*

---

#### What is *__reducer__*?

---

![](img/reducer.png)

---

```jsx
import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'

const reducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT': return { ...state, someValue: state.someValue + 1 }
    case 'DECREMENT': return { ...state, someValue: state.someValue - 1 }
    default: return state
  }
}

const store = createStore(reducer, { someValue: 0 })

const render = () => ReactDOM.render(
  <div>
    <h1>Hi {store.getState().someValue}</h1>
    <button onClick={() => store.dispatch({ type: 'INCREMENT' })}>+</button>
  </div>,
  document.getElementById('root')
)

render()
store.subscribe(render)
```

---

## Simple, right?

<br>
<br>

<small>Not really... but you'll get used to it</small>

---

### Concept

Store + Events<br/> <br/> 

---

### Concept

Store + Events<br/> *+ CQRS*

---

![](img/cycle.png)

---

### How to use it

Demo

---

#### Typical development flow
<!-- Hypothetical -->

After adding new server endpoint...

1. Choose a place to trigger action
1. Write dispatcher for `LOAD_{DATA}` or `{DATA}_REQUESTED`
1. Add event + *middleware* + reducers
1. Handle
  - `{DATA}_LOADING`
  - `{DATA}_LOADED_SUCCESS`
    <small>( empty, has more )</small>
  - `{DATA}_LOADED_FAILURE`
    <small>( offline, 401/404, 500/503, timeout )</small>
1. Deal with component template

---

## Our story

---

## How we use it?

- we started with __*no state management*__
<small style="display:block; margin: .1em 0 .5em 1em">benefits were not clear yet</small>
- slowly adopting `ng-redux`
<small style="display:block; margin: .1em 0 .5em 1em">for communication of separated components</small>
- migrated to Angular 6
  - `@angular-redux/store`
- mostly growing **single store** in `app` module
- moved some parts of overcomplicated components to *__middleware__*
- now... dealing with circular dependencies :/

---

## <small>tl;dr</small>

It was a bumpy road <i class="fa fa-meh"></i>

<!-- Quickly step over next 5 slides -->

---

#### Nevermind... it's done <i class="fa fa-check"></i>

One-time effort.

End of story.

---

### But then... __*<i class="fa fa-skull"></i>*__

<br>

What could go wrong?

---

Surprisingly...

- not about "<i>does not work</i>"
- not about "<i>it's too slow</i>"

*new category of problems*

- unpredictible UI
- so much boilerplate <small>(useless code)</small>
- this architecture sucks <small>(feeling)</small>
- ecosystem sucks <small>(mostly useless snippets)</small>

---

```grid(17:1,transparent)
...
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-lg fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-2x fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-3x fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-2x fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-lg fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
<i class="fa fa-file-code"></i>
&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;
...
```

---

<small>Problem #1</small>

### Too much jumping between files <br><small>increased by TypeScript and Angular</small>

- small change = big PR
- over-complicated mid-size project
- boilerplate for the sake of boilerplate


- gets easier after doing it 20 times
<br><small>when you stop thinking about your next move</small>

---

<small>Problem #2</small>

### When app logic is only partially in Redux

<br>

- __*This is madness!*__
- DevTools cannot reliably undo state?
- App state inconsistent with component state
<br><small>Violation of "single source of truth"</small>

<br>

---

<small>Problem #3</small>

### Race conditions<br><small>when not using effects / middleware</small>

<br>

- Mostly during app initialization
- Loading defaults <small>vs</small> saved query criteria

<br>
<!-- So.. what is the current state we're in? -->
<!-- How does development look like? -->

---

### Objective

Load elements into dropdown

---

How __*hard*__ can it be?

---

## The workflow

1. Mock some values (fake it until you make it)
1. Describe how they look <small>[ HTML, CSS ]</small>
1. Get them from the *__store__*
1. Put them in the *__store__*
1. Load them before sending them to store
   - handle `success` / `error` / `timeout` / `empty`
   - __unsubscribe__ from loading under some conditions
1. Caching? Why load the same stuff twice?
    <!-- in memory, localStorage, HTTP caching -->
1. Find a place to trigger loading event

---

### In other words...

1. edit `my-form.component.ts`
1. edit `my-form.component.html`
1. edit `my-form.component.ts` again
1. declare field in `IMyModuleStore` and set initial value
1. edit `my-module.reducers.ts`
1. edit `my-form.component.ts` again
1. edit `my-module.actions.ts`
1. edit `my-form.component.ts` again
1. edit `my-form.component.html` again
1. ...

---

Demo?
<!-- It's necessary to make them feel this pain -->

---

### Once its done...

### ...you can start thinking about:

- interactions
- handling "reset"
- loading saved queries

---

![](img/mindblown.gif)

---

### So... we keep working on it

<i class="fa fa-wrench"></i> <i class="fa fa-wrench"></i> <i class="fa fa-wrench"></i>

<!-- 
    I am trying to explain the direction fo these efforts
 -->

---

...by moving more logic to *__middleware__*

---

### Middleware to the rescue*?*

---

Not exactly...

---

> > We were not using Redux from the start

Our components became "heavy"
<br><small>(100~300 lines in *.ts files)</small>

<!-- yes, this is "heavy" in components world -->

---

![](pics/dog.jpg)

<!-- and they were asking for more -->

---

### What we get with _middleware_ done right?

- Slightly shorter cycle <small>(files we change)</small>


- *__Less brittle and confusing__* code


- Predictible view layer - dumb (light) components

---

![](pics/wallhaven-447886.png)

---

![](img/hammer.png)

<!-- Middleware is essential to avoid chaos -->

---

## What is *__middleware__*?

---

![](img/middleware-pipeline.png)

---

<div class="pacman">

![](img/pacman.svg)
![](img/pacman.svg)
![](img/pacman.svg)

</div>

---

```jsx
const myMiddleware = store => next => action => {

  // every event pass here

  // 1. does it need our attention?
  // 2. logic (process the command)
  // 3. should we call `next(action)`?

};

export myMiddleware;
```

---

## Demo

<br>

<small>[CodeSandbox.io](https://codesandbox.io)</small>

[search](https://codesandbox.io/search) for "Redux Middleware"

<small>(with Parcel)</small>

<br>

---

```jsx
import { createStore, applyMiddleware } from "redux";
import { myMiddleware } from "./my-middleware";

const store = createStore(
  reducer,
  { },
  applyMiddleware(myMiddleware)
);
```

---

```jsx
const myMiddleware = store => next => action => {
  switch (action.type) {
      case "ADD_VALUE": {
      const lastValue = store.getState().someValue;
      if (lastValue) store.getState().values.push(lastValue);
      store.dispatch({ type: "SET_VALUE", arg: action.arg });
      break;
    }
    case "UNDO": {
      const prevValue = store.getState().values.pop();
      if (prevValue) store.dispatch({ type: "SET_VALUE", arg: prevValue });
      else           store.dispatch({ type: "CLEAR_VALUE" });
      break;
    }
  }
  return next(action);
};
```
---

```jsx
const myMiddleware = store => next => action => {
  switch (action.type) {
    case "FETCH_RANDOM": {
      store.dispatch({ type: "SET_LOADING", arg: true });
      store.dispatch({ type: "CLEAR_ERROR" });

      fetch("/Random")
        .then(response => response.json())
        .then(x =>     store.dispatch({ type: "ADD_VALUE",   arg: x }))
        .finally(() => store.dispatch({ type: "SET_LOADING", arg: false }))
        .catch(() =>   store.dispatch({ type: "SET_ERROR" }));

      break;
    }
  }
  return next(action);
};
```
---

#### Tip #1

> Move **logic** to middleware

---

#### Improved testability

- Unit testing state changes separately from components rendering
- Stuffing store in Cypress tests

---

```js
const store = createStore(
  // reducers

  { values: [], someValue: 42, error: 'Wrong input', loading: true }

  // middleware
);
```

```jsx
return <div>
  <h1>Hi {state.someValue}</h1>
  {state.loading && <em>loading</em>}
  {state.error && <strong>{state.error}</strong>}
</div>
```

---

#### When __*not*__ to use Redux?

- Small projects
- Prototypes
- Static sites (generated)

---

#### When you should adopt Redux?

- When app gets big enough?
- When app gets complex enough?
- Will you have time to re-write?

<br>

---

![](img/start-without-redux.png)

---

<!-- while I don't approve "hype", there is economic argument -->

![](pics/think-code.jpg)

---

#### Tip #2

> *<i class="fa fa-chess-knight"></i>&nbsp; Use Redux from day 1*

- when it is the ~~easiest~~ cheapest
- to avoid mixed logic
- adopt mindset
<br><small>grow dumb components</small>

---

![](img/wisdom.png)

---

#### Remember to configure DevTools

- Redux DevTools
- Logger
- Error monitoring
<!-- https://github.com/netguru/redux-bugsnag-middleware -->

---

#### Tip #3

> Keep it simple (comprehensible)

---

- Merge events, keep less of them to avoid loosing control over your app
- Avoid in-memory caching unless it is really needed
<!-- e.g. loading time ranges for each dataset -->
<!-- ask yourself: is it going to be your client's problem? -->

<hr>

- Consider lighter alternatives ([MobX](https://mobx.js.org/), [ngrx-data](https://github.com/johnpapa/angular-ngrx-data))

- Also... would be nice to have consistent naming

---

#### There is more... <br><small>Questions with no clear answers</small>

- When (and how) should we split to modules?
- Should we initialize store from `localStorage`?
- How it works with Server-Side Rendering?
- Is it useful for offline-first apps?
- Where is the boundary for component state?

<!-- https://jaysoo.ca/2016/02/28/organizing-redux-application/#rule-2-create-strict-module-boundaries -->

<br>

---

### Redux tips - summary

&nbsp;<i class="fa fa-arrow-right"></i>&nbsp; Adopt early

&nbsp;<i class="fa fa-arrow-right"></i>&nbsp; Make good use of middlewares

&nbsp;<i class="fa fa-arrow-right"></i>&nbsp; Make components predictible

&nbsp;<i class="fa fa-arrow-right"></i>&nbsp; Keep it simple

<br>

---

### Resources

<br>

- <i class="fas fa-book-open"></i> [Docs](https://redux.js.org)
- <i class="fas fa-code"></i> [Examples](https://github.com/reduxjs/redux/tree/master/examples)
- <i class="fas fa-graduation-cap"></i> [Courses on Pluralsight](https://www.pluralsight.com/search?q=redux)
- <i class="fab fa-youtube"></i> [NGXS - Angular State Management](https://youtu.be/SGj11j4hxmg)
- <i class="fab fa-youtube"></i> [LevelUpTuts | What Is Redux?](https://youtu.be/mhqO-VL6U2I)
- <i class="fab fa-slideshare"></i> [Data flow architecture in Angular with Redux](https://www.slideshare.net/nirkaufman/angular-redux)
- <i class="fab fa-medium"></i> [10 Tips for Better Redux Architecture](https://medium.com/javascript-scene/10-tips-for-better-redux-architecture-69250425af44)

<br>

---

<br>
<br>
<br>

> [redux-done-right.surge.sh](http://redux-done-right.surge.sh)

<br>
<br>
<br>