import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

const myMiddleware = store => next => action => {
  switch (action.type) {

    case "FETCH_RANDOM": {
      store.dispatch({ type: "SET_LOADING", arg: true });
      store.dispatch({ type: "CLEAR_ERROR" });

      fetch("https://www.random.org/integers/?num=1&min=1&max=2000&col=1&base=10&format=plain&rnd=new", { credentials: "same-origin", mode: "cors" })
        .then(response => response.json())
        .then(x => parseInt(x))
        .then(x => store.dispatch({ type: "ADD_VALUE", arg: x }))
        .finally(() => store.dispatch({ type: "SET_LOADING", arg: false }))
        .catch(() => store.dispatch({ type: "SET_ERROR" }));
      break;
    }

    case "ADD_VALUE": {
      const lastValue = store.getState().someValue;
      if (lastValue) store.getState().values.push(lastValue);
      store.dispatch({ type: "SET_VALUE", arg: action.arg });
      break;
    }

    case "UNDO": {
      const prevValue = store.getState().values.pop();
      if (prevValue) store.dispatch({ type: "SET_VALUE", arg: prevValue });
      else           store.dispatch({ type: "CLEAR_VALUE" });
      break;
    }
  }
  return next(action);
};

const loggingMiddleware = storeAPI => next => action => {
    console.log('dispatching', action)
    let result = next(action)
    console.log('next state', storeAPI.getState())
    return result
};

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_VALUE"  : return { ...state, someValue: action.arg };
    case "CLEAR_VALUE": return { ...state, someValue: '' };

    case "SET_LOADING": return { ...state, loading: action.arg };

    case "SET_ERROR"  : return { ...state, error: 'Something wrong happened with loading random year' };
    case "CLEAR_ERROR": return { ...state, error: undefined };

    default: return state;
  }
};

const store = createStore(
  reducer,
  { values: [] }, // initial state
  compose(composeWithDevTools(applyMiddleware(myMiddleware, loggingMiddleware)))
);

const render = () => {
  const state = store.getState();
  return ReactDOM.render(
    <div>
      <h1>Hi {state.someValue}</h1>
      {state.loading && <em>loading</em>}
      {state.error && <strong>{state.error}</strong>}
      <br /><button onClick={() => store.dispatch({ type: "FETCH_RANDOM" })}>Load new random</button>
      <br /><button onClick={() => store.dispatch({ type: "UNDO" })}>Undo</button>
    </div>,
    document.getElementById("root")
  );
};

render();
store.subscribe(render);