import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'

const reducer = (state = { someValue: 0 }, action) => {
  switch (action.type) {
    case 'INCREMENT': return { ...state, someValue: state.someValue + 1 }
    case 'DECREMENT': return { ...state, someValue: state.someValue - 1 }
    default: return state
  }
}

const store = createStore(reducer)

const render = () => ReactDOM.render(
  <div>
    <h1>Hi {store.getState().someValue}</h1>
    <button onClick={() => store.dispatch({ type: 'INCREMENT' })}>+</button>
  </div>,
  document.body
)

render()
store.subscribe(render)