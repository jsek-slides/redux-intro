class ImpactExtension {

    constructor() {
        this.options = {
            position: `top`,
            height: `5px`,
            color: 'cyan',
        };
    }

    init() {
        const _resize = (...args) => setTimeout(() => this.resize(...args), 100);

        this.renderAll();
        _resize();

        big.onNext(slideDiv => {
            _resize([...slideDiv.querySelectorAll('.impact')])
        });

        big.onModeChange(mode => {
            if (/(jump|print)/.test(mode)) {
                _resize();
            }
        });

        return Promise.resolve();
    }

    renderAll() {
        [...document.body.querySelectorAll('[class^="language-impact"]')]
        .map(element => {
            this.ImpactDiv = document.createElement("div");
            this.ImpactDiv.className = 'impact' + element.className
                .replace(/language\-impact/, '')
                .replace(',', ' ');
            element.parentNode.parentNode.appendChild(this.ImpactDiv);
            this.ImpactDiv.innerHTML = element
                .innerText
                .split('\n')
                .filter(x => !!x)
                .map(x => `<span class='line' style="
                    white-space: nowrap;
                    line-height: .95em;
                    ${x.length > 15 ? 'letter-spacing: 0.05em;' : ''}
                ">${x}</span>`)
                .join('\n');

            element.parentNode.style = 'display: none';
        });
    }

    resize(elements) {
        (elements || [...document.body.querySelectorAll('.impact')])
        .map(impactDiv => {
            let slideContainer = impactDiv.parentNode.parentNode;
            let slideDiv = impactDiv.parentNode;
            this.adjust(
                slideDiv,
                [...impactDiv.querySelectorAll('.line')],
                slideContainer.clientWidth,
                slideContainer.clientHeight,
            );
        });
    }

    adjust(slideDiv, elements, width, height, canRepeat = true) {
        elements
        .map(element => {
            element.style.fontSize = '0px';
            return element;
        })
        .map(element => {
            let fontSize = height;
            [100, 50, 10, 2].forEach(function (step) {
                for (; fontSize > 0; fontSize -= step) {
                    element.style.fontSize = fontSize + 'px';
                    if (slideDiv.offsetWidth <= width) {
                        break;
                    }
                }
                fontSize += step;
            });
        });

        if (canRepeat && slideDiv.offsetHeight > height) {
            let adjustedWidth = Math.floor(width * height / slideDiv.offsetHeight);
            slideDiv.parentNode.style.width = adjustedWidth + 'px';
            this.adjust(slideDiv, elements, adjustedWidth, height, false);
        }
    }
}

window.impact = new ImpactExtension();